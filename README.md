# Dispatcher #

Simple, to the point routing for Leads, Tasks, Cases, and Opportunities in Saleforce.


### Salesforce App Exchange Listing: https://appexchange.salesforce.com/appxListingDetail?listingId=a0N3A00000FvMvsUAF ###


### Dispatcher Basics ###

* Object Settings: https://youtu.be/2GYexr46SGw 
* Daily Reset Settings: https://youtu.be/5qlHvwDOoCE  
* Priority Field: https://youtu.be/ss8VQcFA1m4 
* Preserve Owner: https://youtu.be/zszCMT2DY1g 
* Filters and Filter Logic: https://youtu.be/0lz3_ZeyHSo 
* Members (assignees): https://youtu.be/oDX7P33d3DI 

### Getting Started With Dispatcher - PDF ###

* PDF with quick getting started tutorial: https://bitbucket.org/kuzman05/dispatcher-kb/src/master/Getting_started_with_Dispatcher.pdf

<object data="https://bitbucket.org/kuzman05/dispatcher-kb/src/master/Getting_started_with_Dispatcher.pdf" type="application/pdf" width="800px" height="1000px">
    <p>This browser does not support PDFs. Please download the PDF to view it: <a href="https://bitbucket.org/kuzman05/dispatcher-kb/src/master/Getting_started_with_Dispatcher.pdf">Download PDF</a>.</p>
</object>